import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MqttService } from './services/mqtt.service';
import { SocketService } from './services/socket.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, MqttService, SocketService],
})
export class AppModule {}
