import { Inject, Injectable, OnModuleInit, forwardRef } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  connect,
  IClientOptions,
  IClientSubscribeOptions,
  MqttClient,
} from 'mqtt';
import { debug, error, info } from 'ps-logger';
import { SocketService } from './socket.service';

@Injectable()
export class MqttService implements OnModuleInit {
  constructor(
    @Inject(forwardRef(() => SocketService))
    private socketService: SocketService
  ) {} //  // // ,

  private mqttClient: MqttClient;

  mqttConfig = {
    url: 'http://dc-mq-stag-v2.flytbase.com/',
    username: 'fb_dji_cloud_bridge',
    password: 'fb_dji_cloud_bridge123',
    protocol: 'mqtt',
    clientId: Date.now() + `_${Math.floor(Math.random() * 1000)}_`,
    orgId: '64c0ced104420a78716647b0',
    dockId: '653141ee020b2dd71f560044',
    droneId: '6540b62df66642cb7b73c916',
  };

  topicConfig = {
    goToLocation: {
      request: 'navigation/go_to_location/request',
      response: 'navigation/go_to_location/request_response',
    },
    dronePosition: {
      response: 'global_position',
    },
    dockWeather: {
      response: 'weather',
    },
  };

  events = {
    dockWeather: 'dockWeather',
    dronePosition: 'dronePosition',
    goToLocation: 'goToLocation',
  };

  onModuleInit() {
    this.mqttClient = connect(this.mqttConfig.url, {
      clientId: this.mqttConfig.clientId,
      username: this.mqttConfig.username,
      password: this.mqttConfig.password,
      protocol: 'mqtt',
    });

    this.mqttClient.on('connect', (data) => {
      console.log('Connected to CloudMQTT');
      console.log('MQTT data: ', data);
      this.socketService.sendMessage('onMessage', 'Connected to MQTT');
      this.socketService.sendMessage('onMessage', data);
      const drone = `${this.mqttConfig.orgId}/${this.mqttConfig.droneId}/`;
      const dock = `${this.mqttConfig.orgId}/${this.mqttConfig.dockId}/`;
      const topic = dock + this.topicConfig.dockWeather.response;
      this.subscribeToTopic(
        drone + this.topicConfig.dockWeather.response,
        this.events.dockWeather
      );
      this.subscribeToTopic(
        drone + this.topicConfig.dronePosition.response,
        this.events.dronePosition
      );

      this.socketService.sendMessage(
        'onMessage',
        `subscribed to topic: ${dock + this.topicConfig.dockWeather.respone}`
      );

      this.socketService.sendMessage(
        'onMessage',
        `subscribed to topic: ${dock + this.topicConfig.dronePosition.response}`
      );

      this.publishToTopic(this.events.goToLocation, {
        timestamp: new Date().getTime(),
        job_id: '1223',
        data: {
          latitude: 30.333,
          longitude: 75.333,
          height: 25,
          speed: 2,
          flight_id: 224,
          rth_height: 2,
        },
      });
    });
  }

  subscribeToTopic(topicName: string, event: string) {
    console.log({ topicName });

    let subscribeOptions: IClientSubscribeOptions = {
      qos: 0,
    };
    this.mqttClient.subscribe(topicName, subscribeOptions, (err, grant) => {
      if (err) {
        console.log(err);
      }

      this.mqttClient.on('message', (topic, message, packet) => {
        if (topic === topicName) {
          const responseMessage = {
            topic,
            event,
            message: JSON.parse(message.toString()),
          };
          this.socketService.sendMessage('onMessage', responseMessage);
        }
      });
    });
  }

  unSubscribeFromTopic(topicName: string) {
    this.mqttClient.unsubscribe(topicName, () => {
      console.log('unsubscribed from topic: ', topicName);
      this.socketService.sendMessage(
        'onMessage',
        `unsubscribed from topic: ${topicName}`
      );
    });
  }

  publishToTopic(topicName: string, data: any) {
    console.log('pubTT', topicName, data, this.topicConfig[topicName].request);
    const topic = `${this.mqttConfig.orgId}/${this.mqttConfig.droneId}/${this.topicConfig[topicName].request}`;
    this.mqttClient.publish(topic, Buffer.from(JSON.stringify(data)));
  }
}
