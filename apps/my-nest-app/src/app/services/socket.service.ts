import { Inject, Injectable, OnModuleInit, forwardRef } from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { MqttService } from './mqtt.service';

@WebSocketGateway({
  cors: {
    origin: 'http://localhost:4200',
  },
})
export class SocketService
  implements OnModuleInit, OnGatewayConnection, OnGatewayDisconnect
{
  constructor(
    @Inject(forwardRef(() => MqttService))
    private mqttService: MqttService
  ) {}

  @WebSocketServer()
  server: Server;

  onModuleInit() {
    this.server.on('connection', (socket) => {
      console.log(socket.id);
    });
  }

  handleConnection(client: any) {
    console.log('connected', client.id);
    this.sendMessage('onMessage', 'You are connected!');
  }

  handleDisconnect(client: any) {
    console.log('disconnected: ', client.id);
  }

  @SubscribeMessage('message')
  handleMessage(
    @MessageBody() data: string,
    @ConnectedSocket() client: Socket
  ): void {
    this.sendMessage('onMessage', data);
  }

  @SubscribeMessage('publishGoToLocation')
  handlePublishGoToLocation(@MessageBody() data: any): void {
    this.mqttService.publishToTopic('goToLocation', data);
  }

  sendMessage(event, message) {
    console.log(`Event: ${event}`);
    console.log(`Message: ${JSON.stringify(message)}`);
    this.server.emit(event, message);
  }
}
