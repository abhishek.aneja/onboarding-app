import { Component, OnInit } from '@angular/core';
import { CesiumService } from '../cesium.service';
import { SocketService } from '../services/socket.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'onboarding-app-cesium-component',
  templateUrl: './cesium-component.component.html',
  styleUrls: ['./cesium-component.component.css'],
})
export class CesiumComponentComponent implements OnInit {
  constructor(
    private cesiumService: CesiumService,
    private socketService: SocketService
  ) {}

  messageSocketConnected$!: Observable<any>;
  droneControlForm!: FormData;
  dronePosition$!: Observable<any>;

  // payload: any = {
  //   job_id: 222,
  //   data: {
  //     latitude: 75.55,
  //     longitude: 80.22,
  //     height: 25,
  //     speed: 2,
  //     flight_id: 1234,
  //     rth_height: 2,
  //   },
  // };

  ngOnInit(): void {
    this.cesiumService.plotPoints('cesium');
    // this.cesiumService.plotPoint({ latitude: -80.5, longitude: 35.14 });
    this.messageSocketConnected$ = this.socketService.messageSocketConnected$;

    this.socketService.getMessage();
    this.socketService.getDronePosition().subscribe((data) => {
      // this.cesiumService.plotPoint(data.latitude)
      const latitude = data?.position?.latitude;
      const longitude = data?.position?.longitude;

      console.log(data?.position?.latitude, data?.position?.longitude);
      // this.cesiumService.plotPoint({ latitude, longitude });
    });
  }

  goToLocation(): void {
    this.socketService.publishData('goToLocation', {
      timeStamp: new Date().getTime(),
      job_id: 225,
      data: {
        latitude: 30.55,
        longitude: 76.22,
        height: 25,
        speed: 2,
        flight_id: 1234,
        rth_height: 2,
      },
    });

    this.cesiumService.plotPoint({ latitude: 30.55, longitude: 76.22 });
    this.cesiumService.fly({ latitude: 30.55, longitude: 76.22 });
  }

  connectToDroneLocation(): void {
    this.dronePosition$ = this.socketService.getDronePosition();

    this.socketService.getDronePosition().subscribe((data) => {
      console.log('dronePosition', data);
      // this.cesiumService.plotPoint(this.droneControlForm.value);
    });
  }
}
