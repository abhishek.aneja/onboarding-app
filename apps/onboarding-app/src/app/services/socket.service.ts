import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, filter, map } from 'rxjs';
import { io } from 'socket.io-client';

@Injectable()
export class SocketService {
  socket = io('http://localhost:3000');

  private message: BehaviorSubject<any> = new BehaviorSubject(null);
  message$ = this.message.asObservable();

  private messageSocketConnected: BehaviorSubject<any> = new BehaviorSubject({
    connected: false,
  });
  messageSocketConnected$ = this.messageSocketConnected.asObservable();

  events = {
    dronePosition: 'dronePosition',
    dockWeather: 'dockWeather',
    goToLocation: 'goToLocation',
  };

  getMessage() {
    this.socket.on('onMessage', (message) => {
      this.message.next(message);
      console.log(message);
    });
    this.messageSocketConnected.next({ connected: true });
  }

  stopMessage(): void {
    this.socket.off('onMessage');
    this.messageSocketConnected.next({ connected: false });
  }

  getWeather(): Observable<any> {
    return this.message$.pipe(
      filter(this.filterEvent(this.events.dockWeather)),
      map((data) => {
        return {
          ...data.message.weather,
          timestamp: data.message.timestamp * 1000,
        };
      })
    );
  }

  getDronePosition(): Observable<any> {
    return this.message$.pipe(
      filter(this.filterEvent(this.events.dronePosition)),
      map((data) => {
        return {
          ...data.message,
          timestamp: data.message.timestamp * 1000,
        };
      })
    );
  }

  publishData(eventName: string, payload: any) {
    this.socket.emit(eventName, payload);
  }

  filterEvent(event: string) {
    return function (data: any) {
      if (data?.event !== event) {
        return false;
      }
      return true;
    };
  }
}
