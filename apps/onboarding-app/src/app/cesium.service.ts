import { Injectable } from '@angular/core';

declare let Cesium: any;
Cesium.Ion.defaultAccessToken =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3MDc5YzBiMy1kYjcxLTQzMWMtYjE0Yy1kMzVhYjIwNWFkNmEiLCJpZCI6MTcyOTM1LCJpYXQiOjE2OTc3OTY3NTF9.JzAuvtqtI_0C5TLusCleUpw1ArjF9rUTpD96jNGA7pg';

@Injectable({
  providedIn: 'root',
})
export class CesiumService {
  private viewer: any;
  constructor() {}

  plotPoints(div: string) {
    this.viewer = new Cesium.Viewer(div);
    this.viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(-75.59777, 40.03883),
      point: {
        color: Cesium.Color.RED,
        pixelSize: 16,
      },
    });
    this.viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(-80.5, 35.14),
      point: {
        color: Cesium.Color.BLUE,
        pixelSize: 16,
      },
    });
    this.viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(-80.12, 25.46),
      point: {
        color: Cesium.Color.YELLOW,
        pixelSize: 16,
      },
    });
  }

  fly(position: any) {
    this.viewer.scene.camera.flyTo({
      destination: Cesium.Cartesian3.fromDegrees(
        position.longitude,
        position.latitude,
        15000.0
      ),
    });
  }

  plotPoint(position: any) {
    this.viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(
        position.longitude,
        position.latitude
      ),
      point: {
        color: Cesium.Color.BLUE,
        pixelSize: 16,
      },
    });

    this.fly(position);
  }
}
