import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';
import { NxWelcomeComponent } from './nx-welcome.component';
import { CesiumComponentComponent } from './cesium-component/cesium-component.component';
import { SocketService } from './services/socket.service';

@NgModule({
  declarations: [AppComponent, NxWelcomeComponent, CesiumComponentComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { initialNavigation: 'enabledBlocking' }),
  ],
  providers: [SocketService],
  bootstrap: [AppComponent],
})
export class AppModule {}
